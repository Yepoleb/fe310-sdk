# FE310 SDK

Device headers for the SiFive FE310-G002 generated from device description
files. Doesn't use the official files because I wrote this before searching
for existing solutions, which I regret now.

## Structure

* definitions/: Definition files
* generated/: Generated headers
* freedomheaders.py: Generation script
* template.h: Header template

## What is mapped?

| Address    | Name        | Mapped In |
| ---------- | ----------- | --------- |
| 0x02000000 | CLINT       | skipped   |
| 0x0C000000 | PLIC        | skipped   |
| 0x10000000 | AON         | aon.xml   |
| 0x10008000 | PRCI        | prci.xml  |
| 0x10010000 | OTP Control |           |
| 0x10012000 | GPIO        | gpio.xml  |
| 0x10013000 | UART 0      | uart.xml  |
| 0x10014000 | QSPI 0      | spi.xml   |
| 0x10015000 | PWM 0       | pwm.xml   |
| 0x10016000 | I2C 0       | i2c.xml   |
| 0x10023000 | UART 1      | uart.xml  |
| 0x10024000 | SPI 1       | spi.xml   |
| 0x10025000 | PWM 1       | pwm.xml   |
| 0x10034000 | SPI 2       | spi.xml   |
| 0x10035000 | PWM 2       | pwm.xml   |

## Generating the headers

```sh
python3 freedomheaders.py
```

## Requirements

* Python >= 3.5 probably
* Jinja2

## License

* Code & data: MIT
* Generated files: Public domain / CC0
