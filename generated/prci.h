#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct prci
{
    volatile uint32_t* const p;
} prci;

const prci prci0 = {(uint32_t*)(0x10008000ul)};


/*
 * Register prci_hfrosccfg at byte offset 0x00
 */

#define PRCI_HFROSCCFG_OFFSET 0

typedef struct prci_hfrosccfg
{
    int hfroscdiv;
    int hfrosctrim;
    bool hfroscen;
    bool hfroscrdy;
} prci_hfrosccfg;

static inline prci_hfrosccfg prci_hfrosccfg_get(void)
{
    prci_hfrosccfg result;
    uint32_t reg_value = prci0.p[0];
    result.hfroscdiv = (int)((reg_value >> 0) & 0x3f);
    result.hfrosctrim = (int)((reg_value >> 16) & 0x1f);
    result.hfroscen = (bool)((reg_value >> 30) & 1);
    result.hfroscrdy = (bool)((reg_value >> 31) & 1);
    return result;
}

static inline void prci_hfrosccfg_set(prci_hfrosccfg values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.hfroscdiv) & 0x3f) << 0;
    reg_value |= ((uint32_t)(values.hfrosctrim) & 0x1f) << 16;
    reg_value |= ((uint32_t)(values.hfroscen) & 1) << 30;
    prci0.p[0] = reg_value;
}

/*
 * Register prci_hfxosccfg at byte offset 0x04
 */

#define PRCI_HFXOSCCFG_OFFSET 1

typedef struct prci_hfxosccfg
{
    bool hfxoscen;
    bool hfxoscrdy;
} prci_hfxosccfg;

static inline prci_hfxosccfg prci_hfxosccfg_get(void)
{
    prci_hfxosccfg result;
    uint32_t reg_value = prci0.p[1];
    result.hfxoscen = (bool)((reg_value >> 30) & 1);
    result.hfxoscrdy = (bool)((reg_value >> 31) & 1);
    return result;
}

static inline void prci_hfxosccfg_set(prci_hfxosccfg values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.hfxoscen) & 1) << 30;
    prci0.p[1] = reg_value;
}

/*
 * Register prci_pllcfg at byte offset 0x08
 */

#define PRCI_PLLCFG_OFFSET 2

typedef struct prci_pllcfg
{
    int pllr;
    int pllf;
    int pllq;
    bool pllsel;
    bool pllrefsel;
    bool pllbypass;
    bool plllock;
} prci_pllcfg;

static inline prci_pllcfg prci_pllcfg_get(void)
{
    prci_pllcfg result;
    uint32_t reg_value = prci0.p[2];
    result.pllr = (int)((reg_value >> 0) & 7);
    result.pllf = (int)((reg_value >> 4) & 0x3f);
    result.pllq = (int)((reg_value >> 10) & 3);
    result.pllsel = (bool)((reg_value >> 16) & 1);
    result.pllrefsel = (bool)((reg_value >> 17) & 1);
    result.pllbypass = (bool)((reg_value >> 18) & 1);
    result.plllock = (bool)((reg_value >> 31) & 1);
    return result;
}

static inline void prci_pllcfg_set(prci_pllcfg values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.pllr) & 7) << 0;
    reg_value |= ((uint32_t)(values.pllf) & 0x3f) << 4;
    reg_value |= ((uint32_t)(values.pllq) & 3) << 10;
    reg_value |= ((uint32_t)(values.pllsel) & 1) << 16;
    reg_value |= ((uint32_t)(values.pllrefsel) & 1) << 17;
    reg_value |= ((uint32_t)(values.pllbypass) & 1) << 18;
    prci0.p[2] = reg_value;
}

/*
 * Register prci_plloutdiv at byte offset 0x0C
 */

#define PRCI_PLLOUTDIV_OFFSET 3

typedef struct prci_plloutdiv
{
    int plloutdiv;
    int plloutdivby1;
} prci_plloutdiv;

static inline prci_plloutdiv prci_plloutdiv_get(void)
{
    prci_plloutdiv result;
    uint32_t reg_value = prci0.p[3];
    result.plloutdiv = (int)((reg_value >> 0) & 0x3f);
    result.plloutdivby1 = (int)((reg_value >> 8) & 0x3f);
    return result;
}

static inline void prci_plloutdiv_set(prci_plloutdiv values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.plloutdiv) & 0x3f) << 0;
    reg_value |= ((uint32_t)(values.plloutdivby1) & 0x3f) << 8;
    prci0.p[3] = reg_value;
}

/*
 * Register prci_procmoncfg at byte offset 0xF0
 */

#define PRCI_PROCMONCFG_OFFSET 60

static inline uint32_t prci_procmoncfg_get(void)
{
    return (uint32_t)prci0.p[60];
}

static inline void prci_procmoncfg_set(uint32_t value)
{
    prci0.p[60] = (uint32_t)value;
}

