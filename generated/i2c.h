#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct i2c
{
    volatile uint32_t* const p;
} i2c;

const i2c i2c0 = {(uint32_t*)(0x10016000ul)};


/*
 * Register i2c_prescaler_lo at byte offset 0x00
 */

#define I2C_PRESCALER_LO_OFFSET 0

static inline uint32_t i2c_prescaler_lo_get(void)
{
    return (uint32_t)i2c0.p[0];
}

static inline void i2c_prescaler_lo_set(uint32_t value)
{
    i2c0.p[0] = (uint32_t)value;
}

/*
 * Register i2c_prescaler_hi at byte offset 0x04
 */

#define I2C_PRESCALER_HI_OFFSET 1

static inline uint32_t i2c_prescaler_hi_get(void)
{
    return (uint32_t)i2c0.p[1];
}

static inline void i2c_prescaler_hi_set(uint32_t value)
{
    i2c0.p[1] = (uint32_t)value;
}

/*
 * Register i2c_control at byte offset 0x08
 */

#define I2C_CONTROL_OFFSET 2

typedef struct i2c_control
{
    bool coreen;
    bool inten;
} i2c_control;

static inline i2c_control i2c_control_get(void)
{
    i2c_control result;
    uint32_t reg_value = i2c0.p[2];
    result.coreen = (bool)((reg_value >> 6) & 1);
    result.inten = (bool)((reg_value >> 7) & 1);
    return result;
}

static inline void i2c_control_set(i2c_control values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.coreen) & 1) << 6;
    reg_value |= ((uint32_t)(values.inten) & 1) << 7;
    i2c0.p[2] = reg_value;
}

/*
 * Register i2c_data at byte offset 0x0C
 */

#define I2C_DATA_OFFSET 3

static inline uint32_t i2c_data_get(void)
{
    return (uint32_t)i2c0.p[3];
}

static inline void i2c_data_set(uint32_t value)
{
    i2c0.p[3] = (uint32_t)value;
}

/*
 * Register i2c_cmd at byte offset 0x10
 */

#define I2C_CMD_OFFSET 4

typedef struct i2c_cmd
{
    bool irqack;
    bool ack;
    bool write;
    bool read;
    bool stop;
    bool start;
} i2c_cmd;

static inline void i2c_cmd_set(i2c_cmd values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.irqack) & 1) << 0;
    reg_value |= ((uint32_t)(values.ack) & 1) << 3;
    reg_value |= ((uint32_t)(values.write) & 1) << 4;
    reg_value |= ((uint32_t)(values.read) & 1) << 5;
    reg_value |= ((uint32_t)(values.stop) & 1) << 6;
    reg_value |= ((uint32_t)(values.start) & 1) << 7;
    i2c0.p[4] = reg_value;
}

/*
 * Register i2c_status at byte offset 0x10
 */

#define I2C_STATUS_OFFSET 4

typedef struct i2c_status
{
    bool irqflag;
    bool transferinprogress;
    bool arblost;
    bool busy;
    bool receivedack;
} i2c_status;

static inline i2c_status i2c_status_get(void)
{
    i2c_status result;
    uint32_t reg_value = i2c0.p[4];
    result.irqflag = (bool)((reg_value >> 0) & 1);
    result.transferinprogress = (bool)((reg_value >> 1) & 1);
    result.arblost = (bool)((reg_value >> 5) & 1);
    result.busy = (bool)((reg_value >> 6) & 1);
    result.receivedack = (bool)((reg_value >> 7) & 1);
    return result;
}

