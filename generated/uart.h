#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct uart
{
    volatile uint32_t* const p;
} uart;

const uart uart0 = {(uint32_t*)(0x10013000ul)};
const uart uart1 = {(uint32_t*)(0x10023000ul)};


/*
 * Register uart_txdata at byte offset 0x00
 */

#define UART_TXDATA_OFFSET 0

typedef struct uart_txdata
{
    char data;
    bool full;
} uart_txdata;

static inline uart_txdata uart_txdata_get(uart dev)
{
    uart_txdata result;
    uint32_t reg_value = dev.p[0];
    result.data = (char)((reg_value >> 0) & 0xff);
    result.full = (bool)((reg_value >> 31) & 1);
    return result;
}

static inline void uart_txdata_set(uart dev, uart_txdata values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(unsigned char)(values.data) & 0xff) << 0;
    dev.p[0] = reg_value;
}

/*
 * Register uart_rxdata at byte offset 0x04
 */

#define UART_RXDATA_OFFSET 1

typedef struct uart_rxdata
{
    char data;
    bool empty;
} uart_rxdata;

static inline uart_rxdata uart_rxdata_get(uart dev)
{
    uart_rxdata result;
    uint32_t reg_value = dev.p[1];
    result.data = (char)((reg_value >> 0) & 0xff);
    result.empty = (bool)((reg_value >> 31) & 1);
    return result;
}

/*
 * Register uart_txctrl at byte offset 0x08
 */

#define UART_TXCTRL_OFFSET 2

typedef struct uart_txctrl
{
    bool txen;
    int nstop;
    int txcnt;
} uart_txctrl;

static inline uart_txctrl uart_txctrl_get(uart dev)
{
    uart_txctrl result;
    uint32_t reg_value = dev.p[2];
    result.txen = (bool)((reg_value >> 0) & 1);
    result.nstop = (int)((reg_value >> 1) & 1);
    result.txcnt = (int)((reg_value >> 16) & 7);
    return result;
}

static inline void uart_txctrl_set(uart dev, uart_txctrl values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.txen) & 1) << 0;
    reg_value |= ((uint32_t)(values.nstop) & 1) << 1;
    reg_value |= ((uint32_t)(values.txcnt) & 7) << 16;
    dev.p[2] = reg_value;
}

/*
 * Register uart_rxctrl at byte offset 0x0C
 */

#define UART_RXCTRL_OFFSET 3

typedef struct uart_rxctrl
{
    bool rxen;
    int rxcnt;
} uart_rxctrl;

static inline uart_rxctrl uart_rxctrl_get(uart dev)
{
    uart_rxctrl result;
    uint32_t reg_value = dev.p[3];
    result.rxen = (bool)((reg_value >> 0) & 1);
    result.rxcnt = (int)((reg_value >> 16) & 7);
    return result;
}

static inline void uart_rxctrl_set(uart dev, uart_rxctrl values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.rxen) & 1) << 0;
    reg_value |= ((uint32_t)(values.rxcnt) & 7) << 16;
    dev.p[3] = reg_value;
}

/*
 * Register uart_ie at byte offset 0x10
 */

#define UART_IE_OFFSET 4

typedef struct uart_ie
{
    bool txwm;
    bool rxwm;
} uart_ie;

static inline uart_ie uart_ie_get(uart dev)
{
    uart_ie result;
    uint32_t reg_value = dev.p[4];
    result.txwm = (bool)((reg_value >> 0) & 1);
    result.rxwm = (bool)((reg_value >> 1) & 1);
    return result;
}

static inline void uart_ie_set(uart dev, uart_ie values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.txwm) & 1) << 0;
    reg_value |= ((uint32_t)(values.rxwm) & 1) << 1;
    dev.p[4] = reg_value;
}

/*
 * Register uart_ip at byte offset 0x14
 */

#define UART_IP_OFFSET 5

typedef struct uart_ip
{
    bool txwm;
    bool rxwm;
} uart_ip;

static inline uart_ip uart_ip_get(uart dev)
{
    uart_ip result;
    uint32_t reg_value = dev.p[5];
    result.txwm = (bool)((reg_value >> 0) & 1);
    result.rxwm = (bool)((reg_value >> 1) & 1);
    return result;
}

/*
 * Register uart_div at byte offset 0x18
 */

#define UART_DIV_OFFSET 6

static inline uint32_t uart_div_get(uart dev)
{
    return (uint32_t)dev.p[6];
}

static inline void uart_div_set(uart dev, uint32_t value)
{
    dev.p[6] = (uint32_t)value;
}

