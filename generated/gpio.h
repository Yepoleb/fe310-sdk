#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct gpio
{
    volatile uint32_t* const p;
} gpio;

const gpio gpio0 = {(uint32_t*)(0x10012000ul)};


/*
 * Register gpio_input_val at byte offset 0x00
 */

#define GPIO_INPUT_VAL_OFFSET 0

static inline uint32_t gpio_input_val_get(void)
{
    return (uint32_t)gpio0.p[0];
}

static inline void gpio_input_val_set(uint32_t value)
{
    gpio0.p[0] = (uint32_t)value;
}

/*
 * Register gpio_input_en at byte offset 0x04
 */

#define GPIO_INPUT_EN_OFFSET 1

static inline uint32_t gpio_input_en_get(void)
{
    return (uint32_t)gpio0.p[1];
}

static inline void gpio_input_en_set(uint32_t value)
{
    gpio0.p[1] = (uint32_t)value;
}

/*
 * Register gpio_output_en at byte offset 0x08
 */

#define GPIO_OUTPUT_EN_OFFSET 2

static inline uint32_t gpio_output_en_get(void)
{
    return (uint32_t)gpio0.p[2];
}

static inline void gpio_output_en_set(uint32_t value)
{
    gpio0.p[2] = (uint32_t)value;
}

/*
 * Register gpio_output_val at byte offset 0x0C
 */

#define GPIO_OUTPUT_VAL_OFFSET 3

static inline uint32_t gpio_output_val_get(void)
{
    return (uint32_t)gpio0.p[3];
}

static inline void gpio_output_val_set(uint32_t value)
{
    gpio0.p[3] = (uint32_t)value;
}

/*
 * Register gpio_pue at byte offset 0x10
 */

#define GPIO_PUE_OFFSET 4

static inline uint32_t gpio_pue_get(void)
{
    return (uint32_t)gpio0.p[4];
}

static inline void gpio_pue_set(uint32_t value)
{
    gpio0.p[4] = (uint32_t)value;
}

/*
 * Register gpio_ds at byte offset 0x14
 */

#define GPIO_DS_OFFSET 5

static inline uint32_t gpio_ds_get(void)
{
    return (uint32_t)gpio0.p[5];
}

static inline void gpio_ds_set(uint32_t value)
{
    gpio0.p[5] = (uint32_t)value;
}

/*
 * Register gpio_rise_ie at byte offset 0x18
 */

#define GPIO_RISE_IE_OFFSET 6

static inline uint32_t gpio_rise_ie_get(void)
{
    return (uint32_t)gpio0.p[6];
}

static inline void gpio_rise_ie_set(uint32_t value)
{
    gpio0.p[6] = (uint32_t)value;
}

/*
 * Register gpio_rise_ip at byte offset 0x1C
 */

#define GPIO_RISE_IP_OFFSET 7

static inline uint32_t gpio_rise_ip_get(void)
{
    return (uint32_t)gpio0.p[7];
}

static inline void gpio_rise_ip_set(uint32_t value)
{
    gpio0.p[7] = (uint32_t)value;
}

/*
 * Register gpio_fall_ie at byte offset 0x20
 */

#define GPIO_FALL_IE_OFFSET 8

static inline uint32_t gpio_fall_ie_get(void)
{
    return (uint32_t)gpio0.p[8];
}

static inline void gpio_fall_ie_set(uint32_t value)
{
    gpio0.p[8] = (uint32_t)value;
}

/*
 * Register gpio_fall_ip at byte offset 0x24
 */

#define GPIO_FALL_IP_OFFSET 9

static inline uint32_t gpio_fall_ip_get(void)
{
    return (uint32_t)gpio0.p[9];
}

static inline void gpio_fall_ip_set(uint32_t value)
{
    gpio0.p[9] = (uint32_t)value;
}

/*
 * Register gpio_high_ie at byte offset 0x28
 */

#define GPIO_HIGH_IE_OFFSET 10

static inline uint32_t gpio_high_ie_get(void)
{
    return (uint32_t)gpio0.p[10];
}

static inline void gpio_high_ie_set(uint32_t value)
{
    gpio0.p[10] = (uint32_t)value;
}

/*
 * Register gpio_high_ip at byte offset 0x2C
 */

#define GPIO_HIGH_IP_OFFSET 11

static inline uint32_t gpio_high_ip_get(void)
{
    return (uint32_t)gpio0.p[11];
}

static inline void gpio_high_ip_set(uint32_t value)
{
    gpio0.p[11] = (uint32_t)value;
}

/*
 * Register gpio_low_ie at byte offset 0x30
 */

#define GPIO_LOW_IE_OFFSET 12

static inline uint32_t gpio_low_ie_get(void)
{
    return (uint32_t)gpio0.p[12];
}

static inline void gpio_low_ie_set(uint32_t value)
{
    gpio0.p[12] = (uint32_t)value;
}

/*
 * Register gpio_low_ip at byte offset 0x34
 */

#define GPIO_LOW_IP_OFFSET 13

static inline uint32_t gpio_low_ip_get(void)
{
    return (uint32_t)gpio0.p[13];
}

static inline void gpio_low_ip_set(uint32_t value)
{
    gpio0.p[13] = (uint32_t)value;
}

/*
 * Register gpio_out_xor at byte offset 0x40
 */

#define GPIO_OUT_XOR_OFFSET 16

static inline uint32_t gpio_out_xor_get(void)
{
    return (uint32_t)gpio0.p[16];
}

static inline void gpio_out_xor_set(uint32_t value)
{
    gpio0.p[16] = (uint32_t)value;
}

