#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct spi
{
    volatile uint32_t* const p;
} spi;

const spi qspi0 = {(uint32_t*)(0x10014000ul)};
const spi spi1 = {(uint32_t*)(0x10024000ul)};
const spi spi2 = {(uint32_t*)(0x10034000ul)};

typedef enum spi_phase
{
    SPI_INACTIVE_0 = 0,
    SPI_INACTIVE_1 = 1,
} spi_phase;
typedef enum spi_polarity
{
    SPI_SAMPLE_LEADING = 0,
    SPI_SAMPLE_TRAILING = 1,
} spi_polarity;
typedef enum spi_csmode
{
    SPI_CSMODE_AUTO = 0,
    SPI_CSMODE_HOLD = 2,
    SPI_CSMODE_OFF = 3,
} spi_csmode;
typedef enum spi_proto
{
    SPI_PROTO_SINGLE = 0,
    SPI_PROTO_DUAL = 1,
    SPI_PROTO_QUAD = 2,
} spi_proto;
typedef enum spi_endian
{
    SPI_MSB_FIRST = 0,
    SPI_LSB_FIRST = 1,
} spi_endian;
typedef enum spi_direction
{
    SPI_DIR_RX = 0,
    SPI_DIR_TX = 1,
} spi_direction;

/*
 * Register spi_sckdiv at byte offset 0x00
 */

#define SPI_SCKDIV_OFFSET 0

static inline int spi_sckdiv_get(spi dev)
{
    return (int)dev.p[0];
}

static inline void spi_sckdiv_set(spi dev, int value)
{
    dev.p[0] = (uint32_t)value;
}

/*
 * Register spi_sckmode at byte offset 0x04
 */

#define SPI_SCKMODE_OFFSET 1

typedef struct spi_sckmode
{
    spi_phase pha;
    spi_polarity pol;
} spi_sckmode;

static inline spi_sckmode spi_sckmode_get(spi dev)
{
    spi_sckmode result;
    uint32_t reg_value = dev.p[1];
    result.pha = (spi_phase)((reg_value >> 0) & 1);
    result.pol = (spi_polarity)((reg_value >> 1) & 1);
    return result;
}

static inline void spi_sckmode_set(spi dev, spi_sckmode values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.pha) & 1) << 0;
    reg_value |= ((uint32_t)(values.pol) & 1) << 1;
    dev.p[1] = reg_value;
}

/*
 * Register spi_csid at byte offset 0x10
 */

#define SPI_CSID_OFFSET 4

static inline uint32_t spi_csid_get(spi dev)
{
    return (uint32_t)dev.p[4];
}

static inline void spi_csid_set(spi dev, uint32_t value)
{
    dev.p[4] = (uint32_t)value;
}

/*
 * Register spi_csdef at byte offset 0x14
 */

#define SPI_CSDEF_OFFSET 5

static inline uint32_t spi_csdef_get(spi dev)
{
    return (uint32_t)dev.p[5];
}

static inline void spi_csdef_set(spi dev, uint32_t value)
{
    dev.p[5] = (uint32_t)value;
}

/*
 * Register spi_csmode at byte offset 0x18
 */

#define SPI_CSMODE_OFFSET 6

static inline spi_csmode spi_csmode_get(spi dev)
{
    return (spi_csmode)dev.p[6];
}

static inline void spi_csmode_set(spi dev, spi_csmode value)
{
    dev.p[6] = (uint32_t)value;
}

/*
 * Register spi_delay0 at byte offset 0x28
 */

#define SPI_DELAY0_OFFSET 10

typedef struct spi_delay0
{
    int cssck;
    int sckcs;
} spi_delay0;

static inline spi_delay0 spi_delay0_get(spi dev)
{
    spi_delay0 result;
    uint32_t reg_value = dev.p[10];
    result.cssck = (int)((reg_value >> 0) & 0xff);
    result.sckcs = (int)((reg_value >> 16) & 0xff);
    return result;
}

static inline void spi_delay0_set(spi dev, spi_delay0 values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.cssck) & 0xff) << 0;
    reg_value |= ((uint32_t)(values.sckcs) & 0xff) << 16;
    dev.p[10] = reg_value;
}

/*
 * Register spi_delay1 at byte offset 0x2C
 */

#define SPI_DELAY1_OFFSET 11

typedef struct spi_delay1
{
    int intercs;
    int interxfr;
} spi_delay1;

static inline spi_delay1 spi_delay1_get(spi dev)
{
    spi_delay1 result;
    uint32_t reg_value = dev.p[11];
    result.intercs = (int)((reg_value >> 0) & 0xff);
    result.interxfr = (int)((reg_value >> 16) & 0xff);
    return result;
}

static inline void spi_delay1_set(spi dev, spi_delay1 values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.intercs) & 0xff) << 0;
    reg_value |= ((uint32_t)(values.interxfr) & 0xff) << 16;
    dev.p[11] = reg_value;
}

/*
 * Register spi_fmt at byte offset 0x40
 */

#define SPI_FMT_OFFSET 16

typedef struct spi_fmt
{
    spi_proto proto;
    spi_endian endian;
    spi_direction dir;
    int len;
} spi_fmt;

static inline spi_fmt spi_fmt_get(spi dev)
{
    spi_fmt result;
    uint32_t reg_value = dev.p[16];
    result.proto = (spi_proto)((reg_value >> 0) & 3);
    result.endian = (spi_endian)((reg_value >> 2) & 1);
    result.dir = (spi_direction)((reg_value >> 3) & 1);
    result.len = (int)((reg_value >> 16) & 0xf);
    return result;
}

static inline void spi_fmt_set(spi dev, spi_fmt values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.proto) & 3) << 0;
    reg_value |= ((uint32_t)(values.endian) & 1) << 2;
    reg_value |= ((uint32_t)(values.dir) & 1) << 3;
    reg_value |= ((uint32_t)(values.len) & 0xf) << 16;
    dev.p[16] = reg_value;
}

/*
 * Register spi_txdata at byte offset 0x48
 */

#define SPI_TXDATA_OFFSET 18

typedef struct spi_txdata
{
    char data;
    bool full;
} spi_txdata;

static inline spi_txdata spi_txdata_get(spi dev)
{
    spi_txdata result;
    uint32_t reg_value = dev.p[18];
    result.data = (char)((reg_value >> 0) & 0xff);
    result.full = (bool)((reg_value >> 31) & 1);
    return result;
}

static inline void spi_txdata_set(spi dev, spi_txdata values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(unsigned char)(values.data) & 0xff) << 0;
    dev.p[18] = reg_value;
}

/*
 * Register spi_rxdata at byte offset 0x4C
 */

#define SPI_RXDATA_OFFSET 19

typedef struct spi_rxdata
{
    char data;
    bool empty;
} spi_rxdata;

static inline spi_rxdata spi_rxdata_get(spi dev)
{
    spi_rxdata result;
    uint32_t reg_value = dev.p[19];
    result.data = (char)((reg_value >> 0) & 0xff);
    result.empty = (bool)((reg_value >> 31) & 1);
    return result;
}

/*
 * Register spi_txmark at byte offset 0x50
 */

#define SPI_TXMARK_OFFSET 20

static inline int spi_txmark_get(spi dev)
{
    return (int)dev.p[20];
}

static inline void spi_txmark_set(spi dev, int value)
{
    dev.p[20] = (uint32_t)value;
}

/*
 * Register spi_rxmark at byte offset 0x54
 */

#define SPI_RXMARK_OFFSET 21

static inline int spi_rxmark_get(spi dev)
{
    return (int)dev.p[21];
}

static inline void spi_rxmark_set(spi dev, int value)
{
    dev.p[21] = (uint32_t)value;
}

/*
 * Register spi_fctrl at byte offset 0x60
 */

#define SPI_FCTRL_OFFSET 24

static inline bool spi_fctrl_get(spi dev)
{
    return (bool)dev.p[24];
}

static inline void spi_fctrl_set(spi dev, bool value)
{
    dev.p[24] = (uint32_t)value;
}

/*
 * Register spi_ffmt at byte offset 0x64
 */

#define SPI_FFMT_OFFSET 25

typedef struct spi_ffmt
{
    bool cmd_en;
    int addr_len;
    int pad_cnt;
    spi_proto cmd_proto;
    spi_proto addr_proto;
    spi_proto data_proto;
    char cmd_code;
    char pad_codey;
} spi_ffmt;

static inline spi_ffmt spi_ffmt_get(spi dev)
{
    spi_ffmt result;
    uint32_t reg_value = dev.p[25];
    result.cmd_en = (bool)((reg_value >> 0) & 1);
    result.addr_len = (int)((reg_value >> 1) & 7);
    result.pad_cnt = (int)((reg_value >> 4) & 0xf);
    result.cmd_proto = (spi_proto)((reg_value >> 8) & 3);
    result.addr_proto = (spi_proto)((reg_value >> 10) & 3);
    result.data_proto = (spi_proto)((reg_value >> 12) & 3);
    result.cmd_code = (char)((reg_value >> 16) & 0xff);
    result.pad_codey = (char)((reg_value >> 24) & 0xff);
    return result;
}

static inline void spi_ffmt_set(spi dev, spi_ffmt values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.cmd_en) & 1) << 0;
    reg_value |= ((uint32_t)(values.addr_len) & 7) << 1;
    reg_value |= ((uint32_t)(values.pad_cnt) & 0xf) << 4;
    reg_value |= ((uint32_t)(values.cmd_proto) & 3) << 8;
    reg_value |= ((uint32_t)(values.addr_proto) & 3) << 10;
    reg_value |= ((uint32_t)(values.data_proto) & 3) << 12;
    reg_value |= ((uint32_t)(unsigned char)(values.cmd_code) & 0xff) << 16;
    reg_value |= ((uint32_t)(unsigned char)(values.pad_codey) & 0xff) << 24;
    dev.p[25] = reg_value;
}

/*
 * Register spi_ie at byte offset 0x70
 */

#define SPI_IE_OFFSET 28

typedef struct spi_ie
{
    bool txwm;
    bool rxwm;
} spi_ie;

static inline spi_ie spi_ie_get(spi dev)
{
    spi_ie result;
    uint32_t reg_value = dev.p[28];
    result.txwm = (bool)((reg_value >> 0) & 1);
    result.rxwm = (bool)((reg_value >> 1) & 1);
    return result;
}

static inline void spi_ie_set(spi dev, spi_ie values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.txwm) & 1) << 0;
    reg_value |= ((uint32_t)(values.rxwm) & 1) << 1;
    dev.p[28] = reg_value;
}

/*
 * Register spi_ip at byte offset 0x74
 */

#define SPI_IP_OFFSET 29

typedef struct spi_ip
{
    bool txwm;
    bool rxwm;
} spi_ip;

static inline spi_ip spi_ip_get(spi dev)
{
    spi_ip result;
    uint32_t reg_value = dev.p[29];
    result.txwm = (bool)((reg_value >> 0) & 1);
    result.rxwm = (bool)((reg_value >> 1) & 1);
    return result;
}

