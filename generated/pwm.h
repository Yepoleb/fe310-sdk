#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct pwm
{
    volatile uint32_t* const p;
} pwm;

const pwm pwm0 = {(uint32_t*)(0x10015000ul)};
const pwm pwm1 = {(uint32_t*)(0x10025000ul)};
const pwm pwm2 = {(uint32_t*)(0x10035000ul)};


/*
 * Register pwm_pwmcfg at byte offset 0x00
 */

#define PWM_PWMCFG_OFFSET 0

typedef struct pwm_pwmcfg
{
    int pwmscale;
    bool pwmsticky;
    bool pwmzerocmp;
    bool pwmdeglitch;
    bool pwmenalways;
    bool pwmenoneshot;
    bool pwmcmp0center;
    bool pwmcmp1center;
    bool pwmcmp2center;
    bool pwmcmp3center;
    bool pwmcmp0gang;
    bool pwmcmp1gang;
    bool pwmcmp2gang;
    bool pwmcmp3gang;
    bool pwmcmp0ip;
    bool pwmcmp1ip;
    bool pwmcmp2ip;
    bool pwmcmp3ip;
} pwm_pwmcfg;

static inline pwm_pwmcfg pwm_pwmcfg_get(pwm dev)
{
    pwm_pwmcfg result;
    uint32_t reg_value = dev.p[0];
    result.pwmscale = (int)((reg_value >> 0) & 0xf);
    result.pwmsticky = (bool)((reg_value >> 8) & 1);
    result.pwmzerocmp = (bool)((reg_value >> 9) & 1);
    result.pwmdeglitch = (bool)((reg_value >> 10) & 1);
    result.pwmenalways = (bool)((reg_value >> 12) & 1);
    result.pwmenoneshot = (bool)((reg_value >> 13) & 1);
    result.pwmcmp0center = (bool)((reg_value >> 16) & 1);
    result.pwmcmp1center = (bool)((reg_value >> 17) & 1);
    result.pwmcmp2center = (bool)((reg_value >> 18) & 1);
    result.pwmcmp3center = (bool)((reg_value >> 19) & 1);
    result.pwmcmp0gang = (bool)((reg_value >> 24) & 1);
    result.pwmcmp1gang = (bool)((reg_value >> 25) & 1);
    result.pwmcmp2gang = (bool)((reg_value >> 26) & 1);
    result.pwmcmp3gang = (bool)((reg_value >> 27) & 1);
    result.pwmcmp0ip = (bool)((reg_value >> 28) & 1);
    result.pwmcmp1ip = (bool)((reg_value >> 29) & 1);
    result.pwmcmp2ip = (bool)((reg_value >> 30) & 1);
    result.pwmcmp3ip = (bool)((reg_value >> 31) & 1);
    return result;
}

static inline void pwm_pwmcfg_set(pwm dev, pwm_pwmcfg values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.pwmscale) & 0xf) << 0;
    reg_value |= ((uint32_t)(values.pwmsticky) & 1) << 8;
    reg_value |= ((uint32_t)(values.pwmzerocmp) & 1) << 9;
    reg_value |= ((uint32_t)(values.pwmdeglitch) & 1) << 10;
    reg_value |= ((uint32_t)(values.pwmenalways) & 1) << 12;
    reg_value |= ((uint32_t)(values.pwmenoneshot) & 1) << 13;
    reg_value |= ((uint32_t)(values.pwmcmp0center) & 1) << 16;
    reg_value |= ((uint32_t)(values.pwmcmp1center) & 1) << 17;
    reg_value |= ((uint32_t)(values.pwmcmp2center) & 1) << 18;
    reg_value |= ((uint32_t)(values.pwmcmp3center) & 1) << 19;
    reg_value |= ((uint32_t)(values.pwmcmp0gang) & 1) << 24;
    reg_value |= ((uint32_t)(values.pwmcmp1gang) & 1) << 25;
    reg_value |= ((uint32_t)(values.pwmcmp2gang) & 1) << 26;
    reg_value |= ((uint32_t)(values.pwmcmp3gang) & 1) << 27;
    reg_value |= ((uint32_t)(values.pwmcmp0ip) & 1) << 28;
    reg_value |= ((uint32_t)(values.pwmcmp1ip) & 1) << 29;
    reg_value |= ((uint32_t)(values.pwmcmp2ip) & 1) << 30;
    reg_value |= ((uint32_t)(values.pwmcmp3ip) & 1) << 31;
    dev.p[0] = reg_value;
}

/*
 * Register pwm_pwmcount at byte offset 0x08
 */

#define PWM_PWMCOUNT_OFFSET 2

static inline uint32_t pwm_pwmcount_get(pwm dev)
{
    return (uint32_t)dev.p[2];
}

static inline void pwm_pwmcount_set(pwm dev, uint32_t value)
{
    dev.p[2] = (uint32_t)value;
}

/*
 * Register pwm_pwms at byte offset 0x10
 */

#define PWM_PWMS_OFFSET 4

static inline uint32_t pwm_pwms_get(pwm dev)
{
    return (uint32_t)dev.p[4];
}

static inline void pwm_pwms_set(pwm dev, uint32_t value)
{
    dev.p[4] = (uint32_t)value;
}

/*
 * Register pwm_pwmcmp0 at byte offset 0x20
 */

#define PWM_PWMCMP0_OFFSET 8

static inline uint32_t pwm_pwmcmp0_get(pwm dev)
{
    return (uint32_t)dev.p[8];
}

static inline void pwm_pwmcmp0_set(pwm dev, uint32_t value)
{
    dev.p[8] = (uint32_t)value;
}

/*
 * Register pwm_pwmcmp1 at byte offset 0x24
 */

#define PWM_PWMCMP1_OFFSET 9

static inline uint32_t pwm_pwmcmp1_get(pwm dev)
{
    return (uint32_t)dev.p[9];
}

static inline void pwm_pwmcmp1_set(pwm dev, uint32_t value)
{
    dev.p[9] = (uint32_t)value;
}

/*
 * Register pwm_pwmcmp2 at byte offset 0x28
 */

#define PWM_PWMCMP2_OFFSET 10

static inline uint32_t pwm_pwmcmp2_get(pwm dev)
{
    return (uint32_t)dev.p[10];
}

static inline void pwm_pwmcmp2_set(pwm dev, uint32_t value)
{
    dev.p[10] = (uint32_t)value;
}

/*
 * Register pwm_pwmcmp3 at byte offset 0x2C
 */

#define PWM_PWMCMP3_OFFSET 11

static inline uint32_t pwm_pwmcmp3_get(pwm dev)
{
    return (uint32_t)dev.p[11];
}

static inline void pwm_pwmcmp3_set(pwm dev, uint32_t value)
{
    dev.p[11] = (uint32_t)value;
}

