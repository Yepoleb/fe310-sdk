#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef struct aon
{
    volatile uint32_t* const p;
} aon;

const aon aon0 = {(uint32_t*)(0x10000000ul)};


/*
 * Register aon_wdogcfg at byte offset 0x000
 */

#define AON_WDOGCFG_OFFSET 0

typedef struct aon_wdogcfg
{
    int wdogscale;
    bool wdogrsten;
    bool wdogzerocmp;
    bool wdogenalways;
    bool wdogcoreawake;
    bool wdogip0;
} aon_wdogcfg;

static inline aon_wdogcfg aon_wdogcfg_get(void)
{
    aon_wdogcfg result;
    uint32_t reg_value = aon0.p[0];
    result.wdogscale = (int)((reg_value >> 0) & 0xf);
    result.wdogrsten = (bool)((reg_value >> 8) & 1);
    result.wdogzerocmp = (bool)((reg_value >> 9) & 1);
    result.wdogenalways = (bool)((reg_value >> 12) & 1);
    result.wdogcoreawake = (bool)((reg_value >> 13) & 1);
    result.wdogip0 = (bool)((reg_value >> 28) & 1);
    return result;
}

static inline void aon_wdogcfg_set(aon_wdogcfg values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.wdogscale) & 0xf) << 0;
    reg_value |= ((uint32_t)(values.wdogrsten) & 1) << 8;
    reg_value |= ((uint32_t)(values.wdogzerocmp) & 1) << 9;
    reg_value |= ((uint32_t)(values.wdogenalways) & 1) << 12;
    reg_value |= ((uint32_t)(values.wdogcoreawake) & 1) << 13;
    reg_value |= ((uint32_t)(values.wdogip0) & 1) << 28;
    aon0.p[0] = reg_value;
}

/*
 * Register aon_wdogcount at byte offset 0x008
 */

#define AON_WDOGCOUNT_OFFSET 2

static inline uint32_t aon_wdogcount_get(void)
{
    return (uint32_t)aon0.p[2];
}

static inline void aon_wdogcount_set(uint32_t value)
{
    aon0.p[2] = (uint32_t)value;
}

/*
 * Register aon_wdogs at byte offset 0x010
 */

#define AON_WDOGS_OFFSET 4

static inline uint32_t aon_wdogs_get(void)
{
    return (uint32_t)aon0.p[4];
}

static inline void aon_wdogs_set(uint32_t value)
{
    aon0.p[4] = (uint32_t)value;
}

/*
 * Register aon_wdogfeed at byte offset 0x018
 */

#define AON_WDOGFEED_OFFSET 6

static inline uint32_t aon_wdogfeed_get(void)
{
    return (uint32_t)aon0.p[6];
}

static inline void aon_wdogfeed_set(uint32_t value)
{
    aon0.p[6] = (uint32_t)value;
}

/*
 * Register aon_wdogkey at byte offset 0x01C
 */

#define AON_WDOGKEY_OFFSET 7

static inline uint32_t aon_wdogkey_get(void)
{
    return (uint32_t)aon0.p[7];
}

static inline void aon_wdogkey_set(uint32_t value)
{
    aon0.p[7] = (uint32_t)value;
}

/*
 * Register aon_wdogcmp0 at byte offset 0x020
 */

#define AON_WDOGCMP0_OFFSET 8

static inline uint32_t aon_wdogcmp0_get(void)
{
    return (uint32_t)aon0.p[8];
}

static inline void aon_wdogcmp0_set(uint32_t value)
{
    aon0.p[8] = (uint32_t)value;
}

/*
 * Register aon_rtccfg at byte offset 0x040
 */

#define AON_RTCCFG_OFFSET 16

typedef struct aon_rtccfg
{
    int rtcscale;
    bool rtcenalways;
    bool rtcip0;
} aon_rtccfg;

static inline aon_rtccfg aon_rtccfg_get(void)
{
    aon_rtccfg result;
    uint32_t reg_value = aon0.p[16];
    result.rtcscale = (int)((reg_value >> 0) & 0xf);
    result.rtcenalways = (bool)((reg_value >> 12) & 1);
    result.rtcip0 = (bool)((reg_value >> 28) & 1);
    return result;
}

static inline void aon_rtccfg_set(aon_rtccfg values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.rtcscale) & 0xf) << 0;
    reg_value |= ((uint32_t)(values.rtcenalways) & 1) << 12;
    reg_value |= ((uint32_t)(values.rtcip0) & 1) << 28;
    aon0.p[16] = reg_value;
}

/*
 * Register aon_rtccountlo at byte offset 0x048
 */

#define AON_RTCCOUNTLO_OFFSET 18

static inline uint32_t aon_rtccountlo_get(void)
{
    return (uint32_t)aon0.p[18];
}

static inline void aon_rtccountlo_set(uint32_t value)
{
    aon0.p[18] = (uint32_t)value;
}

/*
 * Register aon_rtccounthi at byte offset 0x04C
 */

#define AON_RTCCOUNTHI_OFFSET 19

static inline uint32_t aon_rtccounthi_get(void)
{
    return (uint32_t)aon0.p[19];
}

static inline void aon_rtccounthi_set(uint32_t value)
{
    aon0.p[19] = (uint32_t)value;
}

/*
 * Register aon_rtcs at byte offset 0x050
 */

#define AON_RTCS_OFFSET 20

static inline uint32_t aon_rtcs_get(void)
{
    return (uint32_t)aon0.p[20];
}

static inline void aon_rtcs_set(uint32_t value)
{
    aon0.p[20] = (uint32_t)value;
}

/*
 * Register aon_rtccmp0 at byte offset 0x060
 */

#define AON_RTCCMP0_OFFSET 24

static inline uint32_t aon_rtccmp0_get(void)
{
    return (uint32_t)aon0.p[24];
}

static inline void aon_rtccmp0_set(uint32_t value)
{
    aon0.p[24] = (uint32_t)value;
}

/*
 * Register aon_lfrosccfg at byte offset 0x070
 */

#define AON_LFROSCCFG_OFFSET 28

typedef struct aon_lfrosccfg
{
    int lfroscdiv;
    int lfrosctrim;
    bool lfroscen;
    bool lfroscrdy;
} aon_lfrosccfg;

static inline aon_lfrosccfg aon_lfrosccfg_get(void)
{
    aon_lfrosccfg result;
    uint32_t reg_value = aon0.p[28];
    result.lfroscdiv = (int)((reg_value >> 0) & 0x3f);
    result.lfrosctrim = (int)((reg_value >> 16) & 0x1f);
    result.lfroscen = (bool)((reg_value >> 30) & 1);
    result.lfroscrdy = (bool)((reg_value >> 31) & 1);
    return result;
}

static inline void aon_lfrosccfg_set(aon_lfrosccfg values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.lfroscdiv) & 0x3f) << 0;
    reg_value |= ((uint32_t)(values.lfrosctrim) & 0x1f) << 16;
    reg_value |= ((uint32_t)(values.lfroscen) & 1) << 30;
    aon0.p[28] = reg_value;
}

/*
 * Register aon_lfclkmux at byte offset 0x07C
 */

#define AON_LFCLKMUX_OFFSET 31

typedef struct aon_lfclkmux
{
    int lfextclk_sel;
    int lfextclk_mux_status;
} aon_lfclkmux;

static inline aon_lfclkmux aon_lfclkmux_get(void)
{
    aon_lfclkmux result;
    uint32_t reg_value = aon0.p[31];
    result.lfextclk_sel = (int)((reg_value >> 0) & 1);
    result.lfextclk_mux_status = (int)((reg_value >> 31) & 1);
    return result;
}

static inline void aon_lfclkmux_set(aon_lfclkmux values)
{
    uint32_t reg_value = 0;
    reg_value |= ((uint32_t)(values.lfextclk_sel) & 1) << 0;
    aon0.p[31] = reg_value;
}

/*
 * Register aon_backup_0 at byte offset 0x080
 */

#define AON_BACKUP_0_OFFSET 32

static inline uint32_t aon_backup_0_get(void)
{
    return (uint32_t)aon0.p[32];
}

static inline void aon_backup_0_set(uint32_t value)
{
    aon0.p[32] = (uint32_t)value;
}

/*
 * Register aon_backup_1 at byte offset 0x084
 */

#define AON_BACKUP_1_OFFSET 33

static inline uint32_t aon_backup_1_get(void)
{
    return (uint32_t)aon0.p[33];
}

static inline void aon_backup_1_set(uint32_t value)
{
    aon0.p[33] = (uint32_t)value;
}

/*
 * Register aon_backup_2 at byte offset 0x088
 */

#define AON_BACKUP_2_OFFSET 34

static inline uint32_t aon_backup_2_get(void)
{
    return (uint32_t)aon0.p[34];
}

static inline void aon_backup_2_set(uint32_t value)
{
    aon0.p[34] = (uint32_t)value;
}

/*
 * Register aon_backup_3 at byte offset 0x08C
 */

#define AON_BACKUP_3_OFFSET 35

static inline uint32_t aon_backup_3_get(void)
{
    return (uint32_t)aon0.p[35];
}

static inline void aon_backup_3_set(uint32_t value)
{
    aon0.p[35] = (uint32_t)value;
}

/*
 * Register aon_backup_4 at byte offset 0x090
 */

#define AON_BACKUP_4_OFFSET 36

static inline uint32_t aon_backup_4_get(void)
{
    return (uint32_t)aon0.p[36];
}

static inline void aon_backup_4_set(uint32_t value)
{
    aon0.p[36] = (uint32_t)value;
}

/*
 * Register aon_backup_5 at byte offset 0x094
 */

#define AON_BACKUP_5_OFFSET 37

static inline uint32_t aon_backup_5_get(void)
{
    return (uint32_t)aon0.p[37];
}

static inline void aon_backup_5_set(uint32_t value)
{
    aon0.p[37] = (uint32_t)value;
}

/*
 * Register aon_backup_6 at byte offset 0x098
 */

#define AON_BACKUP_6_OFFSET 38

static inline uint32_t aon_backup_6_get(void)
{
    return (uint32_t)aon0.p[38];
}

static inline void aon_backup_6_set(uint32_t value)
{
    aon0.p[38] = (uint32_t)value;
}

/*
 * Register aon_backup_7 at byte offset 0x09C
 */

#define AON_BACKUP_7_OFFSET 39

static inline uint32_t aon_backup_7_get(void)
{
    return (uint32_t)aon0.p[39];
}

static inline void aon_backup_7_set(uint32_t value)
{
    aon0.p[39] = (uint32_t)value;
}

/*
 * Register aon_backup_8 at byte offset 0x0A0
 */

#define AON_BACKUP_8_OFFSET 40

static inline uint32_t aon_backup_8_get(void)
{
    return (uint32_t)aon0.p[40];
}

static inline void aon_backup_8_set(uint32_t value)
{
    aon0.p[40] = (uint32_t)value;
}

/*
 * Register aon_backup_9 at byte offset 0x0A4
 */

#define AON_BACKUP_9_OFFSET 41

static inline uint32_t aon_backup_9_get(void)
{
    return (uint32_t)aon0.p[41];
}

static inline void aon_backup_9_set(uint32_t value)
{
    aon0.p[41] = (uint32_t)value;
}

/*
 * Register aon_backup_10 at byte offset 0x0A8
 */

#define AON_BACKUP_10_OFFSET 42

static inline uint32_t aon_backup_10_get(void)
{
    return (uint32_t)aon0.p[42];
}

static inline void aon_backup_10_set(uint32_t value)
{
    aon0.p[42] = (uint32_t)value;
}

/*
 * Register aon_backup_11 at byte offset 0x0AC
 */

#define AON_BACKUP_11_OFFSET 43

static inline uint32_t aon_backup_11_get(void)
{
    return (uint32_t)aon0.p[43];
}

static inline void aon_backup_11_set(uint32_t value)
{
    aon0.p[43] = (uint32_t)value;
}

/*
 * Register aon_backup_12 at byte offset 0x0B0
 */

#define AON_BACKUP_12_OFFSET 44

static inline uint32_t aon_backup_12_get(void)
{
    return (uint32_t)aon0.p[44];
}

static inline void aon_backup_12_set(uint32_t value)
{
    aon0.p[44] = (uint32_t)value;
}

/*
 * Register aon_backup_13 at byte offset 0x0B4
 */

#define AON_BACKUP_13_OFFSET 45

static inline uint32_t aon_backup_13_get(void)
{
    return (uint32_t)aon0.p[45];
}

static inline void aon_backup_13_set(uint32_t value)
{
    aon0.p[45] = (uint32_t)value;
}

/*
 * Register aon_backup_14 at byte offset 0x0B8
 */

#define AON_BACKUP_14_OFFSET 46

static inline uint32_t aon_backup_14_get(void)
{
    return (uint32_t)aon0.p[46];
}

static inline void aon_backup_14_set(uint32_t value)
{
    aon0.p[46] = (uint32_t)value;
}

/*
 * Register aon_backup_15 at byte offset 0x0BC
 */

#define AON_BACKUP_15_OFFSET 47

static inline uint32_t aon_backup_15_get(void)
{
    return (uint32_t)aon0.p[47];
}

static inline void aon_backup_15_set(uint32_t value)
{
    aon0.p[47] = (uint32_t)value;
}

/*
 * Register aon_pmuwakeupi0 at byte offset 0x100
 */

#define AON_PMUWAKEUPI0_OFFSET 64

static inline uint32_t aon_pmuwakeupi0_get(void)
{
    return (uint32_t)aon0.p[64];
}

static inline void aon_pmuwakeupi0_set(uint32_t value)
{
    aon0.p[64] = (uint32_t)value;
}

/*
 * Register aon_pmuwakeupi1 at byte offset 0x104
 */

#define AON_PMUWAKEUPI1_OFFSET 65

static inline uint32_t aon_pmuwakeupi1_get(void)
{
    return (uint32_t)aon0.p[65];
}

static inline void aon_pmuwakeupi1_set(uint32_t value)
{
    aon0.p[65] = (uint32_t)value;
}

/*
 * Register aon_pmuwakeupi2 at byte offset 0x108
 */

#define AON_PMUWAKEUPI2_OFFSET 66

static inline uint32_t aon_pmuwakeupi2_get(void)
{
    return (uint32_t)aon0.p[66];
}

static inline void aon_pmuwakeupi2_set(uint32_t value)
{
    aon0.p[66] = (uint32_t)value;
}

/*
 * Register aon_pmuwakeupi3 at byte offset 0x10C
 */

#define AON_PMUWAKEUPI3_OFFSET 67

static inline uint32_t aon_pmuwakeupi3_get(void)
{
    return (uint32_t)aon0.p[67];
}

static inline void aon_pmuwakeupi3_set(uint32_t value)
{
    aon0.p[67] = (uint32_t)value;
}

/*
 * Register aon_pmuwakeupi4 at byte offset 0x110
 */

#define AON_PMUWAKEUPI4_OFFSET 68

static inline uint32_t aon_pmuwakeupi4_get(void)
{
    return (uint32_t)aon0.p[68];
}

static inline void aon_pmuwakeupi4_set(uint32_t value)
{
    aon0.p[68] = (uint32_t)value;
}

/*
 * Register aon_pmuwakeupi5 at byte offset 0x114
 */

#define AON_PMUWAKEUPI5_OFFSET 69

static inline uint32_t aon_pmuwakeupi5_get(void)
{
    return (uint32_t)aon0.p[69];
}

static inline void aon_pmuwakeupi5_set(uint32_t value)
{
    aon0.p[69] = (uint32_t)value;
}

/*
 * Register aon_pmuwakeupi6 at byte offset 0x118
 */

#define AON_PMUWAKEUPI6_OFFSET 70

static inline uint32_t aon_pmuwakeupi6_get(void)
{
    return (uint32_t)aon0.p[70];
}

static inline void aon_pmuwakeupi6_set(uint32_t value)
{
    aon0.p[70] = (uint32_t)value;
}

/*
 * Register aon_pmuwakeupi7 at byte offset 0x11C
 */

#define AON_PMUWAKEUPI7_OFFSET 71

static inline uint32_t aon_pmuwakeupi7_get(void)
{
    return (uint32_t)aon0.p[71];
}

static inline void aon_pmuwakeupi7_set(uint32_t value)
{
    aon0.p[71] = (uint32_t)value;
}

/*
 * Register aon_pmusleepi0 at byte offset 0x120
 */

#define AON_PMUSLEEPI0_OFFSET 72

static inline uint32_t aon_pmusleepi0_get(void)
{
    return (uint32_t)aon0.p[72];
}

static inline void aon_pmusleepi0_set(uint32_t value)
{
    aon0.p[72] = (uint32_t)value;
}

/*
 * Register aon_pmusleepi1 at byte offset 0x124
 */

#define AON_PMUSLEEPI1_OFFSET 73

static inline uint32_t aon_pmusleepi1_get(void)
{
    return (uint32_t)aon0.p[73];
}

static inline void aon_pmusleepi1_set(uint32_t value)
{
    aon0.p[73] = (uint32_t)value;
}

/*
 * Register aon_pmusleepi2 at byte offset 0x128
 */

#define AON_PMUSLEEPI2_OFFSET 74

static inline uint32_t aon_pmusleepi2_get(void)
{
    return (uint32_t)aon0.p[74];
}

static inline void aon_pmusleepi2_set(uint32_t value)
{
    aon0.p[74] = (uint32_t)value;
}

/*
 * Register aon_pmusleepi3 at byte offset 0x12C
 */

#define AON_PMUSLEEPI3_OFFSET 75

static inline uint32_t aon_pmusleepi3_get(void)
{
    return (uint32_t)aon0.p[75];
}

static inline void aon_pmusleepi3_set(uint32_t value)
{
    aon0.p[75] = (uint32_t)value;
}

/*
 * Register aon_pmusleepi4 at byte offset 0x130
 */

#define AON_PMUSLEEPI4_OFFSET 76

static inline uint32_t aon_pmusleepi4_get(void)
{
    return (uint32_t)aon0.p[76];
}

static inline void aon_pmusleepi4_set(uint32_t value)
{
    aon0.p[76] = (uint32_t)value;
}

/*
 * Register aon_pmusleepi5 at byte offset 0x134
 */

#define AON_PMUSLEEPI5_OFFSET 77

static inline uint32_t aon_pmusleepi5_get(void)
{
    return (uint32_t)aon0.p[77];
}

static inline void aon_pmusleepi5_set(uint32_t value)
{
    aon0.p[77] = (uint32_t)value;
}

/*
 * Register aon_pmusleepi6 at byte offset 0x138
 */

#define AON_PMUSLEEPI6_OFFSET 78

static inline uint32_t aon_pmusleepi6_get(void)
{
    return (uint32_t)aon0.p[78];
}

static inline void aon_pmusleepi6_set(uint32_t value)
{
    aon0.p[78] = (uint32_t)value;
}

/*
 * Register aon_pmusleepi7 at byte offset 0x13C
 */

#define AON_PMUSLEEPI7_OFFSET 79

static inline uint32_t aon_pmusleepi7_get(void)
{
    return (uint32_t)aon0.p[79];
}

static inline void aon_pmusleepi7_set(uint32_t value)
{
    aon0.p[79] = (uint32_t)value;
}

/*
 * Register aon_pmuie at byte offset 0x140
 */

#define AON_PMUIE_OFFSET 80

static inline uint32_t aon_pmuie_get(void)
{
    return (uint32_t)aon0.p[80];
}

static inline void aon_pmuie_set(uint32_t value)
{
    aon0.p[80] = (uint32_t)value;
}

/*
 * Register aon_pmucause at byte offset 0x144
 */

#define AON_PMUCAUSE_OFFSET 81

static inline uint32_t aon_pmucause_get(void)
{
    return (uint32_t)aon0.p[81];
}

static inline void aon_pmucause_set(uint32_t value)
{
    aon0.p[81] = (uint32_t)value;
}

/*
 * Register aon_pmusleep at byte offset 0x148
 */

#define AON_PMUSLEEP_OFFSET 82

static inline uint32_t aon_pmusleep_get(void)
{
    return (uint32_t)aon0.p[82];
}

static inline void aon_pmusleep_set(uint32_t value)
{
    aon0.p[82] = (uint32_t)value;
}

/*
 * Register aon_pmukey at byte offset 0x14C
 */

#define AON_PMUKEY_OFFSET 83

static inline uint32_t aon_pmukey_get(void)
{
    return (uint32_t)aon0.p[83];
}

static inline void aon_pmukey_set(uint32_t value)
{
    aon0.p[83] = (uint32_t)value;
}

/*
 * Register aon_SiFiveBandgap at byte offset 0x210
 */

#define AON_SIFIVEBANDGAP_OFFSET 132

static inline uint32_t aon_SiFiveBandgap_get(void)
{
    return (uint32_t)aon0.p[132];
}

static inline void aon_SiFiveBandgap_set(uint32_t value)
{
    aon0.p[132] = (uint32_t)value;
}

/*
 * Register aon_AONCFG at byte offset 0x300
 */

#define AON_AONCFG_OFFSET 192

static inline uint32_t aon_AONCFG_get(void)
{
    return (uint32_t)aon0.p[192];
}

static inline void aon_AONCFG_set(uint32_t value)
{
    aon0.p[192] = (uint32_t)value;
}

